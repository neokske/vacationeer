namespace VacationRequester.Core;

public interface IVacationRequestService
{
    Task<IQueryable<VacationRequest>> GetRequests();
    Task<VacationRequest> AddVacationRequest(VacationRequest vacationRequest);
}

public interface IUserService
{
    Task<IQueryable<User>> GetUsers();
}

public class UserService : IUserService
{
    private readonly VacationeerDbContext _dbContext;

    public UserService(VacationeerDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Task<IQueryable<User>> GetUsers()
    {
        return Task.FromResult(_dbContext.Users.AsQueryable());
    }
}