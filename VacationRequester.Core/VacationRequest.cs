﻿namespace VacationRequester.Core;

public record VacationRequest(DateTime StartDate, DateTime EndDate)
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public DateTime RequestedOn { get; set; } = DateTime.Now;
    public DateTime StartDate { get; set; } = StartDate;
    public DateTime EndDate { get; set; } = EndDate;
    public User User { get; set; }
}