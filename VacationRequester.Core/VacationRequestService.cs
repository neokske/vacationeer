namespace VacationRequester.Core;

public class VacationRequestService : IVacationRequestService
{
    private readonly VacationeerDbContext _dbContext;

    public VacationRequestService(VacationeerDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Task<IQueryable<VacationRequest>> GetRequests()
    {
        return Task.FromResult(_dbContext.VacationRequests.AsQueryable());
    }

    public async Task<VacationRequest> AddVacationRequest(VacationRequest vacationRequest)
    {
        var entityEntry = await _dbContext.VacationRequests.AddAsync(vacationRequest);
        await _dbContext.SaveChangesAsync();
        return entityEntry.Entity;
    }
}