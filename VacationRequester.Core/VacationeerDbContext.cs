using Microsoft.EntityFrameworkCore;

namespace VacationRequester.Core;

public class VacationeerDbContext : DbContext
{
    public DbSet<VacationRequest> VacationRequests { get; set; }
    public DbSet<User> Users { get; set; }

    public VacationeerDbContext(DbContextOptions options) : base(options)
    {
    }
}