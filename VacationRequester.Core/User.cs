namespace VacationRequester.Core;

public record User(string Username)
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public List<VacationRequest> VacationRequests { get; set; } = new();
}