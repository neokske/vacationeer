# Start project with docker
Hieronder een commando voor als je de dotnet sdk niet installed hebt maar wel docker hebt.

```shell
docker run -it --rm -v $(pwd):/app -w /app/VacationRequester -p 8080:5187 mcr.microsoft.com/dotnet/sdk:6.0 dotnet watch run
```