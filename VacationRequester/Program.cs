using Microsoft.EntityFrameworkCore;
using VacationRequester.Core;
using VacationRequester.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSingleton<WeatherForecastService>();
builder.Services.AddDbContext<VacationeerDbContext>(optionsBuilder =>
{
    optionsBuilder.UseSqlite("Data Source=mydb.db;");
});
builder.Services.AddScoped<IVacationRequestService, VacationRequestService>();
builder.Services.AddScoped<IUserService, UserService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<VacationeerDbContext>();

    if (!await dbContext.Users.AnyAsync())
    {
        await dbContext.Users.AddAsync(new User("Koen"));
        await dbContext.SaveChangesAsync();
    }
    
    await dbContext.Database.MigrateAsync();
}

// app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();